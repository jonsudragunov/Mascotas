django==2.0.5
djangorestframework==3.8.2
django-decouple==2.1
djangorestframework-jwt==1.11.0
gunicorn==19.9.0
