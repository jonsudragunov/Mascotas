from django.db import models
from django.contrib.auth.models import User


class Client(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    telephone = models.CharField(max_length=15)
    created_at = models.DateTimeField(auto_now_add=True)




