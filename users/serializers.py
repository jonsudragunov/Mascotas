from rest_framework import serializers
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('pk', 'email', 'username', 'first_name', 'last_name')
        read_only_fields = ['pk', 'username']
