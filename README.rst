Instrucciones y Documentacion
=============================

Requerimientos
--------------

- Docker
- Docker-compose
- git (opcional)

Instalacion
-----------

Despues de descargar el contenido del repositorio ingrese la carpeta donde esta el archivo docker-compose
y en la consola ingrese:

.. code-block:: bash

    $ docker-compose build
    $ docker-compose up

Esto deberia levantar el servidor y ser visible una respuesta de django con las urls disponibles
en http://localhost/

Estructura del proyecto
=======================
Esta compuesto de dos contendores independientes, djangoapi y nginx vinculados por una red bridge
configurada en el archivo docker-compose.

Configuracion Nginx
-------------------
La configurcion del servidor nginx esta disponible en conf/django.conf, funciona como proxy inverso
del contenedor de python, modificando las cabeceras,  que su ves usa gunicorn para servir las peticiones.

Django
------

Existen 3 aplicaciones separadas con sus propios modelos y api endpoints en /pets /users y /appointments


API
===

El resositorio incluye una base de datos con algunos datos de prueba, la credencial de admin es password123.
Sera necesario crear un token para acceder o modificar a algunas areas ingrese a http://localhost/api/token/
y use la credencial de admin.




