from django.db import models
from users.models import User
from pets.models import Pet


class Appointment(models.Model):

    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    pet = models.ForeignKey(Pet, null=False, blank=True, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    date = models.DateTimeField(null=False, blank=False)

    def __str__(self):
        return self.date

