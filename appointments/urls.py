from django.urls import path
from . import views


urlpatterns = [
        path('api/appointments/', views.AppointmentList.as_view())
        ]
