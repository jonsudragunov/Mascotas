from django.urls import path, include
from . import views

urlpatterns = [
        path('api/pets/', views.PetList.as_view()),
        ]