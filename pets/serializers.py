from rest_framework import serializers
from .models import Pet


class PetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pet
        fields = ('pk', 'name', 'gender', 'age', 'description', 'adoption_date', 'user_id')
        read_only_fields = ['pk']

    def validate_name(self, value):
        qs = Pet.objects.filter(title_iexact=value)
        if qs.instance:
            # excluye el propio nombre
            qs = qs.exclude(pk=self.instance.pk)
        if qs.exist():
            # devuelve error si el nombre existe en la db
            raise serializers.ValidationError("The name already exist in our database, must be unique")
        return value