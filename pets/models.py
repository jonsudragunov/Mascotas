from django.db import models
from users.models import User


class Pet(models.Model):
    MALE = 'm'
    FEMALE = 'f'
    GENDER_CHOICES = (
            (MALE, 'Male'),
            (FEMALE, 'Female'),
            )
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, null=False)
    age = models.IntegerField()
    description = models.CharField(max_length=70, null=True, blank=True)
    adoption_date = models.DateField(null=True, blank=True)
    #owner = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

