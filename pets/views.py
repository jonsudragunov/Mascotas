from pets.models import Pet
from pets.serializers import PetSerializer
from rest_framework import generics


class PetList(generics.ListAPIView):
    queryset = Pet.objects.all()
    serializer_class = PetSerializer
